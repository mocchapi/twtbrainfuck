#!/usr/bin/python3
import time

import tweepy
import tweepyauth
try:
	import brainfuck as bf
except:
	import brainfuck.brainfuck as bf
from html import unescape

TIMEOUT = 1*45      # interval between running tweets
CHECKTIME = 60*1    # interval between checking for new mentions
MAXLOOPS = 5000     # max amount of steps a program can use
TWEETCOUNT = 50     # amount of mentions to return

def save_file(tracker):
    # return
    with open('tracker.txt','w') as f:
        f.write(str(tracker))
    print('Saved tracker')

def main():
    try:
        with open('tracker.txt','r') as f:
            tracker = int(f.read())
    except FileNotFoundError:
        save_file(0)
        tracker = 0
    api = tweepyauth.auto_authenticate()
    me = api.me()
    meid = me.id_str
    print(f'Logged in as @{me.screen_name}')

    try:
        mentions = api.mentions_timeline(since_id=tracker,count=TWEETCOUNT)
    except:
        mentions = api.mentions_timeline(count=TWEETCOUNT)

    try:
        while True:
            print(len(mentions),'new tweets found')
            for tweet in mentions:
                tracker = tweet.id_str
                text = unescape(tweet.text)
                interpreter = bf.Brainfuck(fromstring=text,vocal=False)
                print('New request:',text)
                viable = False
                for character in ['>','<','.',',','+','-','[',']']:
                    if character in text:
                        viable = True
                        break
                if not viable:
                    print('No code to run, skipping')
                    continue
                try:
                    print('Running...')
                    starttime = time.time()
                    interpreter.complete(maxloops=MAXLOOPS)
                    timeout = round(time.time() - starttime,6)
                    bfout = interpreter.gettext()
                    if bfout == '':
                        bfout = 'None'
                    out = f'[{timeout}s] "{bfout}"'
                except bf.loopsExceededError:
                    out = f'Your program exceeded the maximum loop count of {MAXLOOPS}'
                    # print(interpreter.getlogs())
                out = f'@{tweet.author.screen_name} {out}'
                print(f'making tweet: "{out}"')
                api.update_status(out, in_reply_to_status_id=tweet.id_str, auto_populate_reply_metadate=True)
                print(f'done, waiting for {TIMEOUT}s')
                time.sleep(TIMEOUT)
            save_file(tracker)
            mentions = api.mentions_timeline(count=TWEETCOUNT, since_id=tracker)
            print(f'Sleeping for {CHECKTIME}')
            time.sleep(CHECKTIME)
    except Exception as e:
        save_file(tracker)
        raise e

if __name__ == '__main__':
    main()
